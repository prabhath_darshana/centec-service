<?php
use Respect\Validation\Validator as v;

session_start();
require_once __DIR__ . '/../vendor/autoload.php';

$app = new \Slim\App([
  'settings' => [
    'displayErrorDetails' => true,
    'db' => [
      'driver' => 'mysql',
      'host' => 'localhost',
      'database' => 'centecerp_dev',
      'username' => 'root',
      'password' => 'root',
      'charset' => 'utf8',
      'collation' => 'utf8_unicode_ci',
      'prifix' => ''
    ],
    'serverName' => "https://localhost",
    'jwtSecretKey' => "Z2#2}%o(8Yp*<OBpv~_td%031v1jl(B]'9v3+RZ@e|Ch01o9D5&0$3nF3?XW/<L"
  ]
]);

//This conde snipet will enable CORS on the slim server
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Token, Content-Type, user')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});
////////////////////////////////////////////////////////

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['db'] = function($container) use ($capsule){
  return $capsule;
};

$container['AuthController'] = function($container){
  return new \App\Controllers\Auth\AuthController($container);
};

$container['PeopleController'] = function($container){
  return new \App\Controllers\People\PeopleController($container);
};

$container['ClientsController'] = function($container){
  return new \App\Controllers\Clients\ClientsController($container);
};

$container['ClientController'] = function($container){
  return new \App\Controllers\Clients\ClientController($container);
};

$container['ProjectController'] = function($container){
  return new \App\Controllers\Projects\ProjectController($container);
};

$container['ProjectsController'] = function($container){
  return new \App\Controllers\Projects\ProjectsController($container);
};

$container['ProjectPlanController'] = function($container){
  return new \App\Controllers\Projects\ProjectPlanController($container);
};

$container['ProjectTaskController'] = function($container){
  return new \App\Controllers\Projects\ProjectTaskController($container);
};

$container['EmployeeController'] = function($container){
  return new \App\Controllers\Employees\EmployeeController($container);
};

$container['EmployeesController'] = function($container){
  return new App\Controllers\Employees\EmployeesController($container);
};

$container['EmpCategoryController'] = function($container){
  return new \App\Controllers\Employees\EmpCategoryController($container);
};

$container['EmployeeWorkController'] = function($container){
  return new \App\Controllers\Employees\EmployeeWorkController($container);
};

$container['SupplierController'] = function($container){
  return new \App\Controllers\Suppliers\SupplierController($container);
};

$container['SuppliersController'] = function($container){
  return new \App\Controllers\Suppliers\SuppliersController($container);
};

$container['SupplierPoController'] = function($container){
  return new \App\Controllers\Suppliers\SupplierPoController($container);
};

$container['UnitController'] = function($container){
  return new \App\Controllers\Units\UnitController($container);
};

$container['validator'] = function($container){
  return new App\Validation\Validator;
};

$container['jwt'] = function($container){
  return new \App\Modules\Auth\JWTGenerator($container);
};

$container['auth'] = function($container){
  return new \App\Modules\Auth\Auth;
};

$container['projectTasks'] = function($container){
  return new \App\Modules\Projects\ProjectTasks($container);
};

$container['jwtAuth'] = function($container){
  return new \Slim\Middleware\JwtAuthentication([
      "environment" => "HTTP_X_TOKEN",
      "header" => "X-Token",
      "attribute" => "jwt",
      "path" => "/",
      "passthrough" =>[
                        "/auth/signin",
                        "/auth/user",
                        "/auth/signup",
                        "/people/updateProfile"
                      ],
      "algorithm" => "HS512",
      "regexp" => "/(.*)/",
      "secret" => $container['settings']['jwtSecretKey']
  ]);
};
$app->add($container->jwtAuth);

v::with('App\\Validation\\Rules\\');

require_once __DIR__ . '/../app/routes.php';
