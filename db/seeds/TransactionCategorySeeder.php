<?php

use Phinx\Seed\AbstractSeed;

class TransactionCategorySeeder extends AbstractSeed
{
    public function run()
    {
      $data = array(
                [
                  'name'=>'Employee Pay Slip',
                  'description'=>'Employee Payment',
                  'type'=>'-'
                ],
                [
                  'name'=>'Supplier Payment',
                  'description'=>'Supplier Payment',
                  'type'=>'-'
                ],
                [
                  'name'=>'Client Recovery', 
                  'description'=>'Recover from client',
                  'type'=>'+'
                ]
              );
      $table = $this->table('mod_trans_category');
      $table->insert($data)->save();
    }
}
