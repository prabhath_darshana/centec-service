<?php

use Phinx\Migration\AbstractMigration;

class ModOrderStatusCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_order_status');
    $table->addColumn('name', 'string', array('limit'=>20))
          ->addColumn('description', 'string', array('limit'=>255))
          ->addTimestamps()
          ->create();
  }
}
