<?php

use Phinx\Migration\AbstractMigration;

class ModSupPoToModSupBaseFkCreate extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_supplier_po');
      $table->addForeignKey('mod_supplier_base_id', 'mod_supplier_base', 'id',
        array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_sup_po'))
            ->save();
    }
}
