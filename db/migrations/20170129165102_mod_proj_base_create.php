<?php

use Phinx\Migration\AbstractMigration;

class ModProjBaseCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_base');
    $table->addColumn('name', 'string', array('limit'=>255))
          ->addColumn('mod_client_base_id', 'integer', array('limit'=>11))
          ->addColumn('estimation', 'decimal', array('precision' => 15, 'scale' => '2', 'default'=>0.00))
          ->addColumn('amount', 'decimal', array('precision' => 15, 'scale' => '2', 'default'=>0.00))
          ->addColumn('location', 'string', array('limit'=>25,  'null'=>true))
          ->addColumn('address_line1', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('address_line2', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('address_line3', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('country', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('tel_office', 'string', array('limit'=>11, 'null'=>true))
          ->addTimestamps()
          ->create();
  }
}
