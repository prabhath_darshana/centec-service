<?php

use Phinx\Migration\AbstractMigration;

class ModWhTransToModTransBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_warehouse_transaction');
    $table->addForeignKey('mod_trans_base_id', 'mod_trans_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_wherehouse_transaction2'))
          ->save();
  }
}
