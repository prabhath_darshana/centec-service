<?php

use Phinx\Migration\AbstractMigration;

class ModUnitsCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_units');
    $table->addColumn('name', 'string', array('limit'=>25))
          ->addColumn('description', 'string', array('limit'=>255, 'null'=>true))
          ->addTimestamps()
          ->create();
  }
}
