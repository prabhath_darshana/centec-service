<?php

use Phinx\Migration\AbstractMigration;

class ModEmpTransToModTransBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_transaction');
    $table->addForeignKey('mod_trans_base_id', 'mod_trans_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_emp_transaction2'))
          ->save();
  }
}
