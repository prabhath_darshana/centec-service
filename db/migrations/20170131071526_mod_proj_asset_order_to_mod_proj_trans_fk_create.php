<?php

use Phinx\Migration\AbstractMigration;

class ModProjAssetOrderToModProjTransFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_asset_order');
    $table->addForeignKey('mod_proj_transactions_id', 'mod_proj_transactions', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_asset_order_transaction'))
          ->save();
  }
}
