<?php

use Phinx\Migration\AbstractMigration;

class ModEmpTransToModEmpBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_transaction');
    $table->addForeignKey('mod_emp_base_id', 'mod_emp_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_emp_transaction1'))
          ->save();
  }
}
