<?php

use Phinx\Migration\AbstractMigration;

class ModSupplierPoCreate extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_supplier_po');
      $table->addColumn('mod_supplier_base_id', 'integer', array('limit'=>11))
            ->addColumn('mod_warehouse_transaction_id', 'integer', array('limit'=>11))
            ->addColumn('quantity', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
            ->addColumn('rate', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
            ->addColumn('date', 'datetime')
            ->addColumn('note', 'string', array('limit'=>500))
            ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
            ->addTimestamps()
            ->create();
    }
}
