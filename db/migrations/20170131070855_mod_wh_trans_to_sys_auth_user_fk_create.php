<?php

use Phinx\Migration\AbstractMigration;

class ModWhTransToSysAuthUserFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_warehouse_transaction');
    $table->addForeignKey('sys_auth_user_id', 'sys_auth_user', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_wh_trans_user'))
          ->save();
  }
}
