<?php

use Phinx\Migration\AbstractMigration;

class SysPeopleProfileTableCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('sys_people_profile');
    $table->addColumn('first_name', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('last_name', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('dob', 'date', array('null'=>true))
          ->addColumn('gender', 'enum', array('values'=>'undefined,male,female', 'default'=>'undefined'))
          ->addColumn('email', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('address_line1', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('address_line2', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('address_line3', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('country', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('tel_mobile', 'string', array('limit'=>11, 'null'=>true))
          ->addColumn('tel_home', 'string', array('limit'=>11, 'null'=>true))
          ->addColumn('tel_office', 'string', array('limit'=>11, 'null'=>true))
          ->addTimestamps()
          ->create();
  }
}
