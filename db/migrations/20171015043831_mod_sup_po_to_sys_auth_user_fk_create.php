<?php

use Phinx\Migration\AbstractMigration;

class ModSupPoToSysAuthUserFkCreate extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_supplier_po');
      $table->addForeignKey('sys_auth_user_id', 'sys_auth_user', 'id',
        array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_sup_po_user'))
            ->save();
    }
}
