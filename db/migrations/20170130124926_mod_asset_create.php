<?php

use Phinx\Migration\AbstractMigration;

class ModAssetCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_asset');
    $table->addColumn('name', 'string', array('limit'=>255))
          ->addColumn('mod_asset_category_id', 'integer', array('limit'=>11))
          ->addColumn('mod_units_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
