<?php

use Phinx\Migration\AbstractMigration;

class ModProjSubcontractTransToModTransBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_subcontract_transaction');
    $table->addForeignKey('mod_proj_subcontract_id', 'mod_proj_subcontract', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_proj_sub_transaction_base'))
          ->save();
  }
}
