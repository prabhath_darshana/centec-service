<?php

use Phinx\Migration\AbstractMigration;

class ModSuppPeopleToSysPeopleProfileFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_supplier_people');
    $table->addForeignKey('sys_people_profile_id', 'sys_people_profile', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_supplier_people_profile'))
          ->save();
  }
}
