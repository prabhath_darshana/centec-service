<?php

use Phinx\Migration\AbstractMigration;

class ModProjSubcontractToModProjBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_subcontract');
    $table->addForeignKey('mod_proj_base_id', 'mod_proj_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_subcontracts'))
          ->save();
  }
}
