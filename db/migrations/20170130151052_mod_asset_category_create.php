<?php

use Phinx\Migration\AbstractMigration;

class ModAssetCategoryCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_asset_category');
    $table->addColumn('name', 'string', array('limit'=>100))
          ->addColumn('description', 'string', array('limit'=>255))
          ->addTimestamps()
          ->create();
  }
}
