<?php

use Phinx\Migration\AbstractMigration;

class ModClientPeopleToSysPeopleProfileFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_client_people');
    $table->addForeignKey('sys_people_profile_id', 'sys_people_profile', 'id',
      array('delete'=>'SET_NULL', 'update'=>'NO_ACTION', 'constraint'=>'fk_client_people_profile'))
          ->save();
  }
}
