<?php

use Phinx\Migration\AbstractMigration;

class ModWhTransCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_warehouse_transaction');
    $table->addColumn('mod_warehouse_item_id', 'integer', array('limit'=>11))
          ->addColumn('mod_trans_base_id', 'integer', array('limit'=>11))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
