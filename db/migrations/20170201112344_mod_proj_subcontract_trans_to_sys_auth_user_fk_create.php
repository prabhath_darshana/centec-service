<?php

use Phinx\Migration\AbstractMigration;

class ModProjSubcontractTransToSysAuthUserFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_subcontract_transaction');
    $table->addForeignKey('sys_auth_user_id', 'sys_auth_user', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_proj_sub_transaction_user'))
          ->save();
  }
}
