<?php

use Phinx\Migration\AbstractMigration;

class ModSupPoToModWhTransFkCreate extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_supplier_po');
      $table->addForeignKey('mod_warehouse_transaction_id', 'mod_warehouse_transaction', 'id',
        array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_sup_po_transaction'))
            ->save();
    }
}
