<?php

use Phinx\Migration\AbstractMigration;

class ModEmpWorkToModPtojBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_work');
    $table->addForeignKey('mod_proj_base_id', 'mod_proj_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_emp_work_project'))
          ->save();
  }
}
