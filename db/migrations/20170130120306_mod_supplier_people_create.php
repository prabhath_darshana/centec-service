<?php

use Phinx\Migration\AbstractMigration;

class ModSupplierPeopleCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_supplier_people');
    $table->addColumn('mod_supplier_base_id', 'integer', array('limit'=>11))
          ->addColumn('designation', 'string', array('limit'=>100, 'null'=>true))
          ->addColumn('sys_people_profile_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
