<?php

use Phinx\Migration\AbstractMigration;

class ModProjSubcontractTransactionCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_subcontract_transaction');
    $table->addColumn('mod_proj_subcontract_id', 'integer', array('limit'=>11))
          ->addColumn('mod_trans_base_id', 'integer', array('limit'=>11))
          ->addColumn('comment', 'string', array('limit'=>255))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
