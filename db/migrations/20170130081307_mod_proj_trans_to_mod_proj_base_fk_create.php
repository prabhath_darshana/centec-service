<?php

use Phinx\Migration\AbstractMigration;

class ModProjTransToModProjBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_transactions');
    $table->addForeignKey('mod_proj_base_id', 'mod_proj_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_transactions1'))
          ->save();
  }
}
