<?php

use Phinx\Migration\AbstractMigration;

class ModEmpTransactionsCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_transaction');
    $table->addColumn('mod_emp_base_id', 'integer', array('limit'=>11))
          ->addColumn('mod_emp_work_id', 'integer', array('limit'=>11, 'null'=>true))
          ->addColumn('mod_trans_base_id', 'integer', array('limit'=>11))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
