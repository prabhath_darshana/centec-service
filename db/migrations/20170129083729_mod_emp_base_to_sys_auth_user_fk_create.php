<?php

use Phinx\Migration\AbstractMigration;

class ModEmpBaseToSysAuthUserFkCreate extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_emp_base');
      $table->addForeignKey('sys_auth_user_id', 'sys_auth_user', 'id',
        array('delete'=>'SET_NULL', 'update'=>'NO_ACTION', 'constraint'=>'fk_emp_user'))
            ->save();
    }
}
