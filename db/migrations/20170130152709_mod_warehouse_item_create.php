<?php

use Phinx\Migration\AbstractMigration;

class ModWarehouseItemCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_warehouse_item');
    $table->addColumn('mod_asset_id', 'integer', array('limit'=>11))
          ->addColumn('mod_supplier_base_id', 'integer', array('limit'=>11))
          ->addColumn('quantity', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
          ->addColumn('rate', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
          ->addColumn('utilized', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
          ->addColumn('mod_order_status_id', 'integer', array('limit'=>11))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
