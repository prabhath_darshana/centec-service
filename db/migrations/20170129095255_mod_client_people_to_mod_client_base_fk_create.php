<?php

use Phinx\Migration\AbstractMigration;

class ModClientPeopleToModClientBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_client_people');
    $table->addForeignKey('mod_client_base_id', 'mod_client_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'NO_ACTION', 'constraint'=>'fk_client_people_details'))
          ->save();
  }
}
