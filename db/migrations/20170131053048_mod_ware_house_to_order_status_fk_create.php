<?php

use Phinx\Migration\AbstractMigration;

class ModWareHouseToOrderStatusFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_warehouse_item');
    $table->addForeignKey('mod_order_status_id', 'mod_order_status', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_wherehouse_orderstatus'))
          ->save();
  }
}
