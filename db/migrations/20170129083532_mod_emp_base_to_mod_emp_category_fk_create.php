<?php

use Phinx\Migration\AbstractMigration;

class ModEmpBaseToModEmpCategoryFkCreate extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_emp_base');
      $table->addForeignKey('mod_emp_category_id', 'mod_emp_category', 'id',
        array('delete'=>'SET_NULL', 'update'=>'NO_ACTION', 'constraint'=>'fk_emp_category'))
            ->save();
    }
}
