<?php

use Phinx\Migration\AbstractMigration;

class ModEmpBaseToSysPeopleProfileFkCreate extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_emp_base');
      $table->addForeignKey('sys_people_profile_id', 'sys_people_profile', 'id',
        array('delete'=>'SET_NULL', 'update'=>'NO_ACTION', 'constraint'=>'fk_emp_profile'))
            ->save();
    }
}
