<?php

use Phinx\Migration\AbstractMigration;

class ModProjSubcontToModSuppBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_subcontract');
    $table->addForeignKey('mod_supplier_base_id', 'mod_supplier_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_subcontract_supplier'))
          ->save();
  }
}
