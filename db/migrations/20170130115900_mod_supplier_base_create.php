<?php

use Phinx\Migration\AbstractMigration;

class ModSupplierBaseCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_supplier_base');
    $table->addColumn('name', 'string', array('limit'=>255))
          ->addColumn('address_line1', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('address_line2', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('address_line3', 'string', array('limit'=>255, 'null'=>true))
          ->addColumn('country', 'string', array('limit'=>100, 'null'=>true))
          ->addColumn('tel_office', 'string', array('limit'=>15, 'null'=>true))
          ->addColumn('fax_office', 'string', array('limit'=>15, 'null'=>true))
          ->addColumn('email', 'string', array('limit'=>100, 'null'=>true))
          ->addTimestamps()
          ->create();
  }
}
