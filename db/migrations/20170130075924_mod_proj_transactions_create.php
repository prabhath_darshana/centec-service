<?php

use Phinx\Migration\AbstractMigration;

class ModProjTransactionsCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_transactions');
    $table->addColumn('mod_proj_base_id', 'integer', array('limit'=>11))
          ->addColumn('mod_trans_base_id', 'integer', array('limit'=>11))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
