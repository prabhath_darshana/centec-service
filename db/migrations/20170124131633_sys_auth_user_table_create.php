<?php

use Phinx\Migration\AbstractMigration;

class SysAuthUserTableCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('sys_auth_user');
    $table->addColumn('username', 'string', array('limit'=>50))
          ->addColumn('password', 'string', array('limit'=>255))
          ->addColumn('email', 'string', array('limit'=>100))
          ->addColumn('sys_people_profile_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->addIndex(array('username', 'email'), array('unique'=>true, 'name'=>'idx_username_email'))
          ->create();
  }
}
