<?php

use Phinx\Migration\AbstractMigration;

class ModEmpWorkToModEmpBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_work');
    $table->addForeignKey('mod_emp_base_id', 'mod_emp_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_emp_work_employee'))
          ->save();
  }
}
