<?php

use Phinx\Migration\AbstractMigration;

class ModTransBaseCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_trans_base');
    $table->addColumn('mod_trans_category_id', 'integer', array('limit'=>11))
          ->addColumn('amount', 'decimal', array('precision' => 15, 'scale' => '2', 'default'=>0.00))
          ->addColumn('date', 'datetime')
          ->addColumn('comments', 'text', array('null'=>true))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
