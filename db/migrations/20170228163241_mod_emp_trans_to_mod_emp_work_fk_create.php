<?php

use Phinx\Migration\AbstractMigration;

class ModEmpTransToModEmpWorkFkCreate extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_emp_transaction');
      $table->addForeignKey('mod_emp_work_id', 'mod_emp_work', 'id',
        array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_emp_work_transactions'))
            ->save();
    }
}
