<?php

use Phinx\Migration\AbstractMigration;

class ModProjPlanCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_plan');
    $table->addColumn('mod_proj_base_id', 'integer', array('limit'=>11))
          ->addColumn('code', 'string', array('limit'=>5))
          ->addColumn('description', 'string', array('limit'=>255))
          ->addTimestamps()
          ->create();
  }
}
