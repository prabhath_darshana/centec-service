<?php

use Phinx\Migration\AbstractMigration;

class ModProjSubContractCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_subcontract');
    $table->addColumn('mod_proj_base_id', 'integer', array('limit'=>11))
          ->addColumn('mod_supplier_base_id', 'integer', array('limit'=>11))
          ->addColumn('work_description', 'text')
          ->addColumn('estimation', 'decimal', array('precision' => 15, 'scale' => '2', 'default'=>0.00))
          ->addColumn('amount', 'decimal', array('precision' => 15, 'scale' => '2', 'default'=>0.00))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
