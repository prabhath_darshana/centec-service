<?php

use Phinx\Migration\AbstractMigration;

class ModProjBaseToModClientBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_base');
    $table->addForeignKey('mod_client_base_id', 'mod_client_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_client'))
          ->save();
  }
}
