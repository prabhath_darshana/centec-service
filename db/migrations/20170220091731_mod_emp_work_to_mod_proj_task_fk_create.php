<?php

use Phinx\Migration\AbstractMigration;

class ModEmpWorkToModProjTaskFkCreate extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_emp_work');
      $table->addForeignKey('mod_proj_task_id', 'mod_proj_task', 'id',
        array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_emp_work_task'))
            ->save();
    }
}
