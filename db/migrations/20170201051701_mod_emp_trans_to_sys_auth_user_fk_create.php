<?php

use Phinx\Migration\AbstractMigration;

class ModEmpTransToSysAuthUserFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_transaction');
    $table->addForeignKey('sys_auth_user_id', 'sys_auth_user', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_emp_transaction_user'))
          ->save();
  }
}
