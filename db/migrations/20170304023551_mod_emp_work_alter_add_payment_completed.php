<?php

use Phinx\Migration\AbstractMigration;

class ModEmpWorkAlterAddPaymentCompleted extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_emp_work');
      $table->addColumn('payment_completed', 'integer', array('limit'=>1, 'default'=>0))
            ->save();
    }
}
