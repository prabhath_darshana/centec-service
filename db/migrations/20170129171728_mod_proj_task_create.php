<?php

use Phinx\Migration\AbstractMigration;

class ModProjTaskCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_task');
    $table->addColumn('mod_proj_plan_id', 'integer', array('limit'=>11))
          ->addColumn('code', 'string', array('limit'=>5))
          ->addColumn('description', 'string', array('limit'=>255))
          ->addColumn('mod_units_id', 'integer', array('limit'=>11))
          ->addColumn('quantity', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
          ->addColumn('rate', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
          ->addColumn('amount', 'decimal', array('precision' => 15, 'scale' => '2', 'default'=>0.00))
          ->addColumn('start_datetime', 'datetime', array('null'=>true))
          ->addColumn('estimate_days', 'integer', array('limit'=>4, 'default'=>0))
          ->addColumn('actual_days', 'integer', array('limit'=>4, 'default'=>0))
          ->addColumn('comments', 'text', array('null'=>true))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
