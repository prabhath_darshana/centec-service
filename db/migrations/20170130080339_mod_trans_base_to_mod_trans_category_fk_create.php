<?php

use Phinx\Migration\AbstractMigration;

class ModTransBaseToModTransCategoryFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_trans_base');
    $table->addForeignKey('mod_trans_category_id', 'mod_trans_category', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_transaction_categories'))
          ->save();
  }
}
