<?php

use Phinx\Migration\AbstractMigration;

class ModEmpCategoryCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_category');
    $table->addColumn('name', 'string', array('limit'=>255))
          ->addTimestamps()
          ->create();
  }
}
