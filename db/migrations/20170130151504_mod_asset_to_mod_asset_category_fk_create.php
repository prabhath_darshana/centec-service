<?php

use Phinx\Migration\AbstractMigration;

class ModAssetToModAssetCategoryFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_asset');
    $table->addForeignKey('mod_asset_category_id', 'mod_asset_category', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_asset_category'))
          ->save();
  }
}
