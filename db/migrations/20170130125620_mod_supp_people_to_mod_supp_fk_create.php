<?php

use Phinx\Migration\AbstractMigration;

class ModSuppPeopleToModSuppFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_supplier_people');
    $table->addForeignKey('mod_supplier_base_id', 'mod_supplier_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_supplier_people'))
          ->save();
  }
}
