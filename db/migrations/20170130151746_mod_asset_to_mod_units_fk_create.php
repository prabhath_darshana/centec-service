<?php

use Phinx\Migration\AbstractMigration;

class ModAssetToModUnitsFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_asset');
    $table->addForeignKey('mod_units_id', 'mod_units', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_asset_units'))
          ->save();
  }
}
