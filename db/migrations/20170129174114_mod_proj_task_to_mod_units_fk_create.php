<?php

use Phinx\Migration\AbstractMigration;

class ModProjTaskToModUnitsFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_task');
    $table->addForeignKey('mod_units_id', 'mod_units', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_task_units'))
          ->save();
  }
}
