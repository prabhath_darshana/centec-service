<?php

use Phinx\Migration\AbstractMigration;

class ModClientPeopleCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_client_people');
    $table->addColumn('mod_client_base_id', 'integer', array('limit'=>11))
          ->addColumn('designation', 'string', array('limit'=>100, 'null'=>true))
          ->addColumn('sys_people_profile_id', 'integer', array('limit'=>11, 'null'=>true))
          ->addTimestamps()
          ->create();
  }
}
