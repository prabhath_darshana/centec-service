<?php

use Phinx\Migration\AbstractMigration;

class ModEmpBaseCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_base');
    $table->addColumn('mod_emp_category_id', 'integer', array('limit'=>11, 'null'=>true))
          ->addColumn('sys_people_profile_id', 'integer', array('limit'=>11, 'null'=>true))
          ->addColumn('pay_per_day', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
          ->addColumn('balance_payment', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11, 'null'=>true))
          ->addTimestamps()
          ->create();
  }
}
