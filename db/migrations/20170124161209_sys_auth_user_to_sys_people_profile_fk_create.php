<?php

use Phinx\Migration\AbstractMigration;

class SysAuthUserToSysPeopleProfileFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('sys_auth_user');
    $table->addForeignKey('sys_people_profile_id', 'sys_people_profile', 'id',
      array('delete'=>'NO_ACTION', 'update'=>'NO_ACTION', 'constraint'=>'fk_user_profile'))
          ->save();
  }
}
