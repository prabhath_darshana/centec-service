<?php

use Phinx\Migration\AbstractMigration;

class ModEmpWorkToSysAuthUserFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_work');
    $table->addForeignKey('sys_auth_user_id', 'sys_auth_user', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_emp_work_user'))
          ->save();
  }
}
