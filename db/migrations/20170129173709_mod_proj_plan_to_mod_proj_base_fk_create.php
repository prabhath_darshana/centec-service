<?php

use Phinx\Migration\AbstractMigration;

class ModProjPlanToModProjBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_plan');
    $table->addForeignKey('mod_proj_base_id', 'mod_proj_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'NO_ACTION', 'constraint'=>'fk_project_plan'))
          ->save();
  }
}
