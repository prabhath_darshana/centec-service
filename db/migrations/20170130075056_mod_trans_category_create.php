<?php

use Phinx\Migration\AbstractMigration;

class ModTransCategoryCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_trans_category');
    $table->addColumn('name', 'string', array('limit'=>100))
          ->addColumn('description', 'string', array('limit'=>255))
          ->addColumn('type', 'string', array('limit'=>2))
          ->addTimestamps()
          ->create();
  }
}
