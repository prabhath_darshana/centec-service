<?php

use Phinx\Migration\AbstractMigration;

class ModEmpWorkAlter extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('mod_emp_work');
      $table->addColumn('mod_proj_task_id', 'integer', array('limit'=>11, 'null'=>true))
            ->save();
    }
}
