<?php

use Phinx\Migration\AbstractMigration;

class ModWhItemToModAssetFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_warehouse_item');
    $table->addForeignKey('mod_asset_id', 'mod_asset', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_wh_asset'))
          ->save();
  }
}
