<?php

use Phinx\Migration\AbstractMigration;

class ModProjAssetOrderToSysAuthUserFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_asset_order');
    $table->addForeignKey('sys_auth_user_id', 'sys_auth_user', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_asset_order_user'))
          ->save();
  }
}
