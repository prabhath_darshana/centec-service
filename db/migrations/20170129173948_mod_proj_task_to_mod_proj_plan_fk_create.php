<?php

use Phinx\Migration\AbstractMigration;

class ModProjTaskToModProjPlanFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_task');
    $table->addForeignKey('mod_proj_plan_id', 'mod_proj_plan', 'id',
      array('delete'=>'RESTRICT', 'update'=>'NO_ACTION', 'constraint'=>'fk_project_plan_tasks'))
          ->save();
  }
}
