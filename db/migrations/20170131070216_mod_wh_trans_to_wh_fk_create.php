<?php

use Phinx\Migration\AbstractMigration;

class ModWhTransToWhFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_warehouse_transaction');
    $table->addForeignKey('mod_warehouse_item_id', 'mod_warehouse_item', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_wherehouse_transaction1'))
          ->save();
  }
}
