<?php

use Phinx\Migration\AbstractMigration;

class ModProjTransToModTransBaseFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_transactions');
    $table->addForeignKey('mod_trans_base_id', 'mod_trans_base', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_transactions2'))
          ->save();
  }
}
