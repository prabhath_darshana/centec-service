<?php

use Phinx\Migration\AbstractMigration;

class ModEmpWorkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_emp_work');
    $table->addColumn('mod_emp_base_id', 'integer', array('limit'=>11))
          ->addColumn('mod_proj_base_id', 'integer', array('limit'=>11))
          ->addColumn('date', 'datetime')
          ->addColumn('work_description', 'string', array('limit'=>255))
          ->addColumn('daily_payable', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
          ->addColumn('day_paid', 'decimal', array('precision' => 10, 'scale' => '2', 'default'=>0.00))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();

  }
}
