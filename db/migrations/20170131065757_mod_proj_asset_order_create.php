<?php

use Phinx\Migration\AbstractMigration;

class ModProjAssetOrderCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_asset_order');
    $table->addColumn('mod_warehouse_item_id', 'integer', array('limit'=>11))
          ->addColumn('mod_proj_base_id', 'integer', array('limit'=>11))
          ->addColumn('mod_proj_transactions_id', 'integer', array('limit'=>11))
          ->addColumn('mod_order_status_id', 'integer', array('limit'=>11))
          ->addColumn('sys_auth_user_id', 'integer', array('limit'=>11))
          ->addTimestamps()
          ->create();
  }
}
