<?php

use Phinx\Migration\AbstractMigration;

class ModProjAssetOrderToOrderStatusFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_asset_order');
    $table->addForeignKey('mod_order_status_id', 'mod_order_status', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_asset_order_status'))
          ->save();
  }
}
