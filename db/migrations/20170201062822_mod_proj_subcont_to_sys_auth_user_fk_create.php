<?php

use Phinx\Migration\AbstractMigration;

class ModProjSubcontToSysAuthUserFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_subcontract');
    $table->addForeignKey('sys_auth_user_id', 'sys_auth_user', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_subcontract_user'))
          ->save();
  }
}
