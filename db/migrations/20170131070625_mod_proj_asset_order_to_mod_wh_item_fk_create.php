<?php

use Phinx\Migration\AbstractMigration;

class ModProjAssetOrderToModWhItemFkCreate extends AbstractMigration
{
  public function change()
  {
    $table = $this->table('mod_proj_asset_order');
    $table->addForeignKey('mod_warehouse_item_id', 'mod_warehouse_item', 'id',
      array('delete'=>'RESTRICT', 'update'=>'CASCADE', 'constraint'=>'fk_project_wherehouse_item'))
          ->save();
  }
}
