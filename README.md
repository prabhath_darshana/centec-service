# README #

Centec service is a rest api that created using php with mysql database in order to give backend support for centec-ui project

### How do I get set up? ###

This project use php composer to initiate. Follow the instruction in following url to setup new environment.
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-14-04

Quick guide:
```
sudo apt-get update
sudo apt-get install curl php5-cli git
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```

This will download and install Composer as a system-wide command named composer, under /usr/local/bin. The output should look like this:

```
Output
#!/usr/bin/env php
All settings correct for using Composer
Downloading...

Composer successfully installed to: /usr/local/bin/composer
Use it: php /usr/local/bin/composer
```

To test your installation, run:
`composer`

When installing a project that already contains a composer.json file,
you need to run composer install in order to download the project's dependencies.

--Init project
`composer install`

--DB migration
http://docs.phinx.org/en/latest/install.html
```
composer require robmorgan/phinx

php vendor/bin/phinx migrate
php vendor/bin/phinx seed:run
```

--
This won't enough for brand new server setup. In order to get this done you'll have to follow these steps additionally. This instructions are for Ubuntu Linux Server configuration.
###This project based on Slim3 php framework. To know more visit Slim website.
https://www.slimframework.com/docs/

###Grant execution permission to public/index.php
`sudo chmod 777 public/index.php`

###install necessary php modules.
`sudo apt install php-[module_name]`

required modules:
bcmath, gd, intl, json, mbstring, mcrypt, mysqli, pdo, phar

###Enable .htaccess file
To do this, edit /etc/apache2/apache2.conf
```
sudo nano /etc/apache2/apache2.conf
```

And make AllowOverride All
```
<Directory /home/prabhathd/www>
	Options Indexes FollowSymLinks
	AllowOverride All
	Require all granted
</Directory>
```
###Enable RewriteEngine
To do this, run following command
`sudo a2enmod rewrite && sudo service apache2 restart`
