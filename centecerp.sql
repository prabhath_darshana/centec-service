-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2017 at 11:15 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `centecerp`
--

-- --------------------------------------------------------

--
-- Table structure for table `sys_auth_roles`
--

CREATE TABLE `sys_auth_roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(45) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_auth_screenroles`
--

CREATE TABLE `sys_auth_screenroles` (
  `id` int(11) NOT NULL,
  `sys_auth_roles_id` int(11) NOT NULL,
  `sys_auth_screens_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_auth_screens`
--

CREATE TABLE `sys_auth_screens` (
  `id` int(11) NOT NULL,
  `screenid` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_auth_user`
--

CREATE TABLE `sys_auth_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sys_people_profile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_auth_user`
--

INSERT INTO `sys_auth_user` (`id`, `username`, `password`, `email`, `created_at`, `updated_at`, `sys_people_profile_id`) VALUES
(1, 'dev', '$2y$10$6h9wzQ2Eqan/DxN8g41Cy.FveI5UoSCEFOqaFHeqqCUJFsMKf4Rju', 'a@e', '2017-01-21 02:34:51', '2017-01-21 02:34:51', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sys_auth_userroles`
--

CREATE TABLE `sys_auth_userroles` (
  `id` int(11) NOT NULL,
  `sys_auth_user_id` int(11) NOT NULL,
  `sys_auth_roles_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_people_profile`
--

CREATE TABLE `sys_people_profile` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_line1` varchar(255) NOT NULL,
  `address_line2` varchar(255) NOT NULL,
  `address_line3` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `tel_mobile` varchar(15) NOT NULL,
  `tel_home` varchar(15) NOT NULL,
  `tel_office` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sys_auth_roles`
--
ALTER TABLE `sys_auth_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_auth_screenroles`
--
ALTER TABLE `sys_auth_screenroles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sys_auth_screenroles_1_idx` (`sys_auth_roles_id`),
  ADD KEY `fk_sys_auth_screenroles_2_idx` (`sys_auth_screens_id`);

--
-- Indexes for table `sys_auth_screens`
--
ALTER TABLE `sys_auth_screens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_auth_user`
--
ALTER TABLE `sys_auth_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_auth_userroles`
--
ALTER TABLE `sys_auth_userroles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sys_auth_userroles_1_idx` (`sys_auth_user_id`),
  ADD KEY `fk_sys_auth_userroles_2_idx` (`sys_auth_roles_id`);

--
-- Indexes for table `sys_people_profile`
--
ALTER TABLE `sys_people_profile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sys_auth_roles`
--
ALTER TABLE `sys_auth_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_auth_screenroles`
--
ALTER TABLE `sys_auth_screenroles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_auth_screens`
--
ALTER TABLE `sys_auth_screens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_auth_user`
--
ALTER TABLE `sys_auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sys_auth_userroles`
--
ALTER TABLE `sys_auth_userroles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_people_profile`
--
ALTER TABLE `sys_people_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `sys_auth_screenroles`
--
ALTER TABLE `sys_auth_screenroles`
  ADD CONSTRAINT `fk_sys_auth_screenroles_1` FOREIGN KEY (`sys_auth_roles_id`) REFERENCES `sys_auth_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sys_auth_screenroles_2` FOREIGN KEY (`sys_auth_screens_id`) REFERENCES `sys_auth_screens` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sys_auth_userroles`
--
ALTER TABLE `sys_auth_userroles`
  ADD CONSTRAINT `fk_sys_auth_userroles_1` FOREIGN KEY (`sys_auth_user_id`) REFERENCES `sys_auth_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sys_auth_userroles_2` FOREIGN KEY (`sys_auth_roles_id`) REFERENCES `sys_auth_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
