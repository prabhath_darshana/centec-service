<?php
namespace App\Modules\Auth;

use Firebase\JWT\JWT;

class JWTGenerator{

  protected $container;

  public function __construct($container){
    $this->container = $container;
  }

  public function create($userid){
    $tokenid = base64_encode(mcrypt_create_iv(32));
    $issuedAt = time();
    $notBefore = $issuedAt + 1;
    $expire = $notBefore + 3600; //1 hour
    $serverName = $this->container['settings']['serverName'];

    $secretKey = $this->container['settings']['jwtSecretKey'];

    $data = [
      'iat' => $issuedAt,
      'jti' => $tokenid,
      'iss' => $serverName,
      'nbf' => $notBefore,
      'exp' => $expire,
      'data' => [
        'userid' => $userid,
      ]
    ];

    $jwt = JWT::encode(
                  $data,
                  $secretKey,
                  'HS512'
                );
    return $jwt;
  }

  public function decode($token){
    $secretKey = $this->container['settings']['jwtSecretKey'];
    return JWT::decode($token, $secretKey, array('HS512'));
  }
}
