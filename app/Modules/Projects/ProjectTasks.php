<?php
namespace App\Modules\Projects;

use App\Modules\Module;

class ProjectTasks extends Module{
  public function getAll($planId){
    $tasks = $this->db::table('mod_proj_task as task')
          ->where('mod_proj_plan_id', '=', $planId)
          ->join('mod_units as units', 'units.id', '=', 'task.mod_units_id')
          ->select('task.id', 'code', 'task.description', 'units.id as unitId', 'units.name as unitName', 'quantity', 'rate', 'amount', 'start_datetime', 'estimate_days', 'actual_days')
          ->get();

    return $tasks;
  }
}
