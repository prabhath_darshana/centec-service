<?php
namespace App\Modules\Employee;

use App\Modules\Module;

use App\Models\Employee\Employee;
use App\Models\Employee\EmpWork;
use App\Models\Employee\EmpTransaction;
use App\Models\Transaction\TransCategory;
use App\Models\Transaction\Transaction;

class Work extends Module{
  public function create($json, $user){
    $this->db::transaction(function() use ($json, $user){
      $category = TransCategory::where('name', '=', 'Employee Pay Slip')->first();

      $sqlDate = date_create($json['date'])->format('Y-m-d');

      $paymentBalance = $json['daily_payable'] - $json['day_paid'];
      $completed = $paymentBalance == 0 ? 1:0;

      $empWork = EmpWork::create([
        'mod_emp_base_id'=>$json['empId'],
        'mod_proj_base_id'=>$json['project']['id'],
        'date'=>$sqlDate,
        'work_description'=>$json['work_description'],
        'daily_payable'=>$json['daily_payable'],
        'day_paid'=>$json['day_paid'],
        'sys_auth_user_id'=>$user[0],
        'payment_completed'=>$completed,
        // 'mod_proj_task_id'=>$json['task'],
      ]);

      $transaction = Transaction::create([
        'mod_trans_category_id'=>$category['id'],
        'amount'=>$json['day_paid'],
        'date'=>$sqlDate,
        'comments'=>$json['work_description'],
        'sys_auth_user_id'=>$user[0]
      ]);

      $empTransaction = EmpTransaction::create([
        'mod_emp_base_id' => $json['empId'],
        'mod_emp_work_id' => $empWork['id'],
        'mod_trans_base_id' => $transaction['id'],
        'sys_auth_user_id' =>$user[0],
      ]);

      $employee = Employee::find($json['empId']);
      $balance = $employee['balance_payment'] + $paymentBalance;
      $employee->update(['balance_payment'=>$balance]);
    });

    $json['status'] = 'success';
    return $json;
  }

  public function update($json, $user){
    $this->db::transaction(function() use ($json, $user){
      $sqlDate = date_create($json['date'])->format('Y-m-d');

      $empWork = EmpWork::find($json['id']);
      $dayPaid = $empWork['day_paid'];
      $oldBalance = $empWork['daily_payable'] - $dayPaid;
      $newBalance = $json['daily_payable'] - $json['day_paid'];

      $completed = $newBalance == 0 ? 1:0;

      $empWork->update([
          'mod_proj_base_id'=>$json['project']['id'],
          'date'=>$sqlDate,
          'work_description'=>$json['work_description'],
          'daily_payable'=>$json['daily_payable'],
          'day_paid'=>$json['day_paid'],
          'sys_auth_user_id'=>$user[0],
          'payment_completed'=>$completed,
          // 'mod_proj_task_id'=>$json['task'],
        ]);

      $employee = Employee::find($empWork['mod_emp_base_id']);
      $balance = ($employee['balance_payment'] - $oldBalance) + $newBalance;
      $employee->update(['balance_payment'=>$balance]);
    });

    $json['status'] = 'success';
    return $json;
  }

  public function delete($json, $user){
    $this->db::transaction(function() use ($json, $user){
      $empWork = EmpWork::find($json['id']);
      $empId = $empWork['mod_emp_base_id'];

      $empTransactions = EmpTransaction::where('mod_emp_work_id', '=', $empWork['id']);

      $transactions = [];
      foreach($empTransactions as $empTransaction){
        $transactions .= array_fill($empTransaction['mod_trans_base_id']);
      }
      $empTransactions->delete();

      foreach($transactions as $transaction){
        $transaction = Transaction::find($empTransaction['mod_trans_base_id']);
        $transaction->delete();
      }

      $empWork->delete();

      $employee = Employee::find($empId);
      $balance = $employee['balance_payment'] - ($json['daily_payable'] - $json['day_paid']);
      $employee->update(['balance_payment'=>$balance]);
    });

    $json['status'] = 'success';
    return $json;
  }

  public function getWorkLog($params){
    $workLog = $this->db::table('mod_emp_work as ew')
                ->join('mod_proj_base as pb', 'pb.id', '=', 'ew.mod_proj_base_id')
                ->select('ew.id', 'date', 'work_description', 'daily_payable', 'payment_completed',
                        'day_paid', 'pb.name as projectName', 'pb.id as projectId')
                ->where('ew.mod_emp_base_id', '=', $params['empId']);

    $today = date("Y-m-d");
    $fromDate = $today;
    if(!empty($params['fromDate'])){
      $fromDate = date_create($params['fromDate'])->format('Y-m-d');
    }
    $toDate = $today;
    if(!empty($params['toDate'])){
      $toDate = date_create($params['toDate'])->format('Y-m-d');
    }
    $workLog->whereBetween('date', [$fromDate, $toDate]);

    if(!empty($params['project'])){
      $workLog->where('pb.id', '=', $params['project']);
    }

    if($params['completed'] == 'true' && $params['incomplete'] == 'true'){
      $workLog->whereIn('payment_completed', [1, 0]);
    }else if($params['completed'] == 'true'){
      $workLog->whereIn('payment_completed', [1]);
    }else if($params['incomplete'] == 'true'){
      $workLog->whereIn('payment_completed', [0]);
    }

    $workLog->orderBy('ew.date', 'desc')
                ->orderBy('ew.id', 'desc');

    return $workLog->get();
  }

  public function doPayment($json, $user){
    $empWork = EmpWork::where('id', '=', $json['id'])->sharedLock()->get()->first();

    $balanace = $empWork['daily_payable'] - $empWork['day_paid'];

    $this->db::transaction(function() use ($empWork, $balanace, $user){
      $category = TransCategory::where('name', '=', 'Employee Pay Slip')->first();

      $sqlDate = date("Y-m-d");

      $transaction = Transaction::create([
        'mod_trans_category_id'=>$category['id'],
        'amount'=>$balanace,
        'date'=>$sqlDate,
        'comments'=>'Complete Payment',
        'sys_auth_user_id'=>$user[0]
      ]);

      $empTransaction = EmpTransaction::create([
        'mod_emp_base_id' => $empWork['mod_emp_base_id'],
        'mod_emp_work_id' => $empWork['id'],
        'mod_trans_base_id' => $transaction['id'],
        'sys_auth_user_id' =>$user[0],
      ]);

      $empWork->update([
        'day_paid' => $empWork['daily_payable'],
        'payment_completed' => 1
      ]);

      $employee = Employee::where('id', '=', $empWork['mod_emp_base_id'])->sharedLock()->get()->first();
      $empbalance = $employee['balance_payment'] - $balanace;
      $employee->update(['balance_payment'=>$empbalance]);
    });

    $json['status'] = 'success';
    return $json;
  }
}
