<?php
namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class EmpCategory extends Model{
  protected $table = 'mod_emp_category';

  protected $fillable = [
    'name',
  ];
}
