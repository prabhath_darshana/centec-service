<?php
namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;
use App\Models\Project\Project;
use App\Models\Employee\EmpTransaction;

class EmpWork extends Model{
  protected $table = 'mod_emp_work';

  protected $fillable = [
    'mod_emp_base_id',
    'mod_proj_base_id',
    'date',
    'work_description',
    'daily_payable',
    'mod_emp_transaction_id',
    'day_paid',
    'sys_auth_user_id',
    'mod_proj_task_id',
    'payment_completed'
  ];

  public function getEmpWork(){
    return [
      'id' => $this->id,
      'project' => $this->project(),
      'date' => $this->date,
      'description' => $this->work_description,
      'daily_payable' => $this->daily_payable,
      'day_paid' => $this->day_paid,
      'emp_transaction' => $this->empTransaction(),
    ];
  }

  public function project(){
    return Project::find($this->mod_proj_base_id);
  }

  public function empTransaction(){
    return EmpTransaction::find($this->mod_emp_transaction_id);
  }
}
