<?php
namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class EmpTransaction extends Model{
  protected $table = 'mod_emp_transaction';

  protected $fillable = [
    'mod_emp_base_id',
    'mod_emp_work_id',
    'mod_trans_base_id',
    'sys_auth_user_id',
  ];
}
