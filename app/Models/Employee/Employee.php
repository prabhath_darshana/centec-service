<?php
namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;
use App\Models\Employee\EmpCategory;
use App\Models\People\Profile;

class Employee extends Model{
  protected $table = 'mod_emp_base';

  protected $fillable = [
    'mod_emp_category_id',
    'sys_people_profile_id',
    'pay_per_day',
    'balance_payment',
    'sys_auth_user_id',
  ];


  public function getEmployee(){
    $employee = [
      'id' => $this->id,
      'profile'=> $this->peopleProfile(),
      'category' => $this->category(),
      'pay_per_day' => $this->pay_per_day,
      'balance_payment' => $this->balance_payment
    ];
    return $employee;
  }
  /*
  * Get Employee Category
  */
  public function category(){
    return EmpCategory::find($this->mod_emp_category_id);
  }

  /*
  * Get the people profile associated with the employee
  */
  public function peopleProfile(){
    // return $this->belongsTo('App\Models\People\Profile', 'sys_people_profile_id');
    return Profile::find($this->sys_people_profile_id);
  }
}
