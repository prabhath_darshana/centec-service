<?php
namespace App\Models\People;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model{
  protected $table = 'sys_people_profile';

  protected $fillable = [
    'first_name',
    'last_name',
    'dob',
    'gender',
    'email',
    'address_line1',
    'address_line2',
    'address_line3',
    'country',
    'tel_mobile',
    'tel_home',
    'tel_office'
  ];
}
