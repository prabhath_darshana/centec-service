<?php
namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

class ClientPeople extends Model{
  protected $table = 'mod_client_people';

  protected $fillable = [
    'mod_client_base_id',
    'designation',
    'sys_people_profile_id',
  ];
}
