<?php
namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

class Client extends Model{
  protected $table = 'mod_client_base';

  protected $fillable = [
    'name',
    'email',
    'address_line1',
    'address_line2',
    'address_line3',
    'country',
    'fax_office',
    'tel_office'
  ];
}
