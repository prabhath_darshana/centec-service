<?php
namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model{
  protected $table = 'mod_supplier_base';

  protected $fillable = [
    'name',
    'email',
    'address_line1',
    'address_line2',
    'address_line3',
    'country',
    'fax_office',
    'tel_office'
  ];
}
