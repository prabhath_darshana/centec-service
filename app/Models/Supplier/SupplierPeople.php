<?php
namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Model;

class SupplierPeople extends Model{
  protected $table = 'mod_supplier_people';

  protected $fillable = [
    'mod_supplier_base_id',
    'designation',
    'sys_people_profile_id',
  ];
}
