<?php
namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectTask extends Model{
  protected $table = "mod_proj_task";

  protected $fillable = [
    'mod_proj_plan_id',
    'code',
    'description',
    'mod_units_id',
    'quantity',
    'rate',
    'amount',
    'start_datetime',
    'estimate_days',
    'actual_days',
    'comments',
    'sys_auth_user_id',
  ];
}
