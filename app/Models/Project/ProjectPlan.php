<?php
namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectPlan extends Model{
  protected $table = "mod_proj_plan";

  protected $fillable = [
    'mod_proj_base_id',
    'code',
    'description',
  ];
}
