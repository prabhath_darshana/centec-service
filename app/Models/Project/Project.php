<?php
namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client\Client;

class Project extends Model{
  protected $table = "mod_proj_base";

  protected $fillable = [
    'name',
    'mod_client_base_id',
    'estimation',
    'amount',
    'location',
    'address_line1',
    'address_line2',
    'address_line3',
    'country',
    'tel_office'
  ];

  public function getProject(){
    $project = [
      'id' => $this->id,
      'name' => $this->name,
      'client' => $this->client(),
      'estimation' => $this->estimation,
      'address_line1' => $this->address_line1,
      'address_line2' => $this->address_line2,
      'address_line3' => $this->address_line3,
      'tel_office' => $this->tel_office
    ];
    return $project;
  }

  public function client(){
    return Client::find($this->mod_client_base_id);
  }
}
