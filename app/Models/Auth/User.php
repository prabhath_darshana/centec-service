<?php
namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class User extends Model{
  protected $table = 'sys_auth_user';

  protected $fillable = [
    'email',
    'username',
    'password',
    'sys_people_profile_id',
  ];

  public function setPassword($password){
    $this->update([
      'password' => password_hash($password, PASSWORD_DEFAULT)
    ]);
  }

  public function getUserData(){
    // $user = $this->db->select('SELECT id, username, email,sys_people_profile_id FROM sys_auth_user WHERE id=?',[1]);
    $user = [
      'id'=>$this->id,
      'username'=>$this->username,
      'email'=>$this->email,
      'profileId'=>$this->sys_people_profile_id
    ];
    return $user;
  }
}
