<?php
namespace App\Models\Unit;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model{
  protected $table = 'mod_units';

  protected $fillable = [
    'name',
    'description'
  ];
}
