<?php
namespace App\Models\transaction;

use Illuminate\Database\Eloquent\Model;

class TransCategory extends Model{
  protected $table = 'mod_trans_category';

  protected $fillable = [
    'name',
    'description',
    'type',
  ];
}
