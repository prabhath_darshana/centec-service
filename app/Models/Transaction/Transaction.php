<?php
namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model{
  protected $table = 'mod_trans_base';

  protected $fillable = [
    'mod_trans_category_id',
    'amount',
    'date',
    'comments',
    'sys_auth_user_id'
  ];
}
