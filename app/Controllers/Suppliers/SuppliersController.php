<?php
namespace App\Controllers\Suppliers;

use App\Controllers\Controller;

class SuppliersController extends Controller{
  public function getAllSuppliers($request, $response){
    $suppliers = $this->db::table('mod_supplier_base')
                      ->select('id', 'name', 'tel_office', 'fax_office', 'email')
                      ->get();
    // $suppliers = $this->db::table('mod_supplier_base as sb')
    //               ->join('mod_supplier_people as sp', 'sp.mod_supplier_base_id', '=', 'sb.id')
    //               ->groupBy('sb.id')
    //               ->select('sb.id',
    //                        'sb.name',
    //                       'sb.tel_office',
    //                       'sb.fax_office',
    //                       $this->db::raw('count(sp.id) as people'))
    //               ->get();
    return json_encode($suppliers);
  }
}
