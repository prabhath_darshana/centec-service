<?php
namespace App\Controllers\People;

use App\Controllers\Controller;
use App\Models\People\Profile;
use App\Models\Client\ClientPeople;
use App\Models\Supplier\SupplierPeople;
use Respect\Validation\Validator as v;

class PeopleController extends Controller{

  public function getProfile($request, $response){
    $profileId = $request->getParam('profileId');
    $profile = Profile::where('id', $profileId)->first();
    return json_encode($profile);
  }

  public function create($request, $response){
    $json = $request->getParsedBody();
    $type = $request->getParam('type');
    $id = $request->getParam('id');

    $response = $json;
    $response = $this->validateInput($request, $response);
    if($response['status'] == 'failed'){
      return json_encode($response);
    }

    $this->db::transaction(function() use ($json, $id, $type){
      $person = Profile::create([
        'first_name' => $json['first_name'],
        'last_name' => $json['last_name'],
        // 'dob' => $json['dob'],
        'email' => $json['email'],
        'address_line1' => $json['address_line1'],
        'address_line2' => $json['address_line2'],
        'address_line3' => $json['address_line3'],
        'tel_home' => $json['tel_home'],
        'tel_mobile' => $json['tel_mobile'],
        'tel_office' => $json['tel_office'],
        // 'gender' => $json['gender']
      ]);

      $asscoPeople = [];
      if($type == 'client'){
        $asscoPeople = ClientPeople::create([
          'mod_client_base_id' => $id,
          'designation' => $json['designation'],
          'sys_people_profile_id' => $person->id,
        ]);
      }else if($type == 'supplier'){
        $asscoPeople = SupplierPeople::create([
          'mod_supplier_base_id' => $id,
          'designation' => $json['designation'],
          'sys_people_profile_id' => $person->id,
        ]);
      }
      $json['id'] = $asscoPeople->id;
      $json['sys_people_profile_id'] = $person->id;
    });

    $response = $json;
    $response['status'] ='success';
    return json_encode($response);
  }

  public function update($request, $response){
    $json = $request->getParsedBody();
    $type = $request->getParam('type');
    $id = $request->getParam('id');

    $response = $json;
    $response = $this->validateInput($request, $response);
    if($response['status'] == 'failed'){
      return json_encode($response);
    }

    $this->db::transaction(function() use ($json, $type){
      $person = Profile::where('id', $json['sys_people_profile_id'])
      ->update([
        'first_name' => $json['first_name'],
        'last_name' => $json['last_name'],
        // 'dob' => $json['dob'],
        'email' => $json['email'],
        'address_line1' => $json['address_line1'],
        'address_line2' => $json['address_line2'],
        'address_line3' => $json['address_line3'],
        'tel_home' => $json['tel_home'],
        'tel_mobile' => $json['tel_mobile'],
        'tel_office' => $json['tel_office'],
        // 'gender' => $json['gender']
      ]);

      if($type == 'client'){
        ClientPeople::where('id', $json['id'])
          ->update(['designation' => $json['designation']]);
      }else if($type == 'supplier'){
        SupplierPeople::where('id', $json['id'])
          ->update(['designation' => $json['designation']]);
      }
    });

    $response['status'] ='success';
    return json_encode($response);
  }

  public function delete($request, $response){
    $json = $request->getParsedBody();
    $type = $request->getParam('type');
    // $id = $request->getParam('id');

    $this->db::transaction(function() use($json, $type){
      $person = [];
      if($type=='client'){
        $person = ClientPeople::find($json['id']);
      }else if($type=='supplier'){
        $person = SupplierPeople::find($json['id']);
      }
      $profile = Profile::find($person['sys_people_profile_id']);
      $person->delete();
      $profile->delete();
    });

    $response = $json;
    $response['status'] ='success';
    return json_encode($response);
  }

  public function updateWithProfilePic($request, $response){
    $json = $request->getParsedBody();
    $files = $request->getUploadedFiles();

    $profile = $this->db::table('sys_people_profile')
                    ->where('id', $json['id'])
                    ->update([
      'first_name' => $json['first_name'],
      'last_name' => $json['last_name'],
      // 'dob' => $json['dob'],
      'email' => $json['email'],
      'address_line1' => $json['address_line1'],
      'address_line2' => $json['address_line2'],
      'address_line3' => $json['address_line3'],
      'tel_home' => $json['tel_home'],
      'tel_mobile' => $json['tel_mobile'],
      'tel_office' => $json['tel_office'],
      'gender' => $json['gender']
    ]);
    // return json_encode($profile);
    if(empty($files)){
      // return json_encode(["status"=>"empty"]);
    }
    $files["profilePicture"]->moveTo("img/img.jpg");
    // return json_encode($files);
    return json_encode($profile);
    // return json_encode($json);
  }

  public function getAllPeopleAssociatedWith($request, $response){
    $type = $request->getParam('type');
    $id = $request->getParam('id');

    // $table = $this->db::table('sys_people_profile');
    if($type == 'client'){
      $table = $this->db::table('mod_client_people as ap')
                    ->where('mod_client_base_id', '=', $id);
    }else if($type == 'supplier'){
      $table = $this->db::table('mod_supplier_people as ap')
                    ->where('mod_supplier_base_id', '=', $id);
    }

    $people = $table->select('ap.id',
                              'ap.sys_people_profile_id',
                              'ap.designation',
                              'pp.first_name',
                              'pp.last_name',
                              'pp.email',
                              'pp.address_line1',
                              'pp.address_line2',
                              'pp.address_line3',
                              'pp.tel_mobile',
                              'pp.tel_home',
                              'pp.tel_office')
                    ->join('sys_people_profile as pp', 'pp.id', '=', 'ap.sys_people_profile_id')
                    ->get();
    return json_encode($people);
  }

  protected function validateInput($request, $response){
    $validation = $this->validator->validate($request, [
      'email' => v::noWhitespace()->email(),
      'first_name' => v::notEmpty()->alpha(),
      'last_name' => v::notEmpty()->alpha(),
      'tel_home' => v::digit(),
      'tel_mobile' => v::digit(),
      'tel_office' => v::digit(),
    ]);

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
    }else{
      $response['status'] ='success';
    }
    return $response;
  }
}
