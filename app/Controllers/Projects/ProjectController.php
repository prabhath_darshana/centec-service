<?php
namespace App\Controllers\Projects;

use App\Controllers\Controller;
use App\Models\Project\Project;
use Respect\Validation\Validator as v;

class ProjectController extends Controller{
  public function get($request, $response){
    $projectId = $request->getParam('projectId');
    $project = Project::find($projectId)->getProject();
    return json_encode($project);
  }

  public function create($request, $response){
    $json = $request->getParsedBody();

    $response = $json;
    $response = $this->validateInputs($request, $response, 'create');
    if($response['status'] == 'failed'){
      return json_encode($response);
    }

    $project = Project::create([
      'name'=>$json['name'],
      'mod_client_base_id'=>$json['client']['originalObject']['id'],
      'estimation'=>$json['estimation'],
      // 'amount',
      // 'location',
      'address_line1'=>$json['address_line1'],
      'address_line2'=>$json['address_line2'],
      'address_line3'=>$json['address_line3'],
      // 'country',
      'tel_office'=>$json['tel_office'],
    ]);

    $response['status'] ='success';
    return json_encode($response);
  }

  public function update($request, $response){
    $json = $request->getParsedBody();

    $response = $json;
    $response = $this->validateInputs($request, $response, 'update');
    if($response['status'] == 'failed'){
      return json_encode($response);
    }

    $project = Project::where('id', $json['id'])
    ->update([
      'name'=>$json['name'],
      // 'mod_client_base_id'=>$json['client']['originalObject']['id'],
      'estimation'=>$json['estimation'],
      // 'amount',
      // 'location',
      'address_line1'=>$json['address_line1'],
      'address_line2'=>$json['address_line2'],
      'address_line3'=>$json['address_line3'],
      // 'country',
      'tel_office'=>$json['tel_office'],
    ]);

    $response['status'] ='success';
    return json_encode($response);
  }

  protected function validateInputs($request, $response, $type){
    if($type=='create'){
      $validation = $this->validator->validate($request, [
        'name' => v::notEmpty()->alpha()->ProjectAvailable(),
        'tel_office' => v::digit(),
        // 'client' =>
      ]);
    }else{
      $validation = $this->validator->validate($request, [
        'name' => v::notEmpty()->alpha(),
        'tel_office' => v::digit(),
        // 'client' =>
      ]);
    }

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
    }else{
      $response['status'] ='success';
    }
    return $response;
  }
}
