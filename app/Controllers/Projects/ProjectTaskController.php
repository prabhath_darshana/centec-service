<?php
namespace App\Controllers\Projects;

use App\Controllers\Controller;
use App\Models\Project\ProjectTask;
use Respect\Validation\Validator as v;

class ProjectTaskController extends Controller{
  public function getAll($request, $response){
    $planId = $request->getParam('planId');
    $tasks = $this->projectTasks->getAll($planId);

    $resp = [
      'tasks' => $tasks
    ];
    return json_encode($resp);
  }

  public function createTask($request, $response){
     $json = $request->getParsedBody();
     $planId = $request->getParam('planId');
     $userid = $request->getHeader('user');

     $response = $json;
     $response = $this->validateInputs($request, $response);
     if($response['status'] == 'failed'){
       return json_encode($response);
     }

     $projectTask = ProjectTask::create([
       'mod_proj_plan_id' => $planId,
       'code' => $json['code'],
       'description' => $json['description'],
       'mod_units_id' => $json['unit']['id'],
       'quantity' => $json['quantity'],
       'rate' => $json['rate'],
       'estimate_days' => $json['estimate_days'],
       'sys_auth_user_id' => $userid[0]
     ]);

     $response = $projectTask;
     $response['status'] = 'success';
     return json_encode($response);
  }

  public function updateTask($request, $response){
    $json = $request->getParsedBody();
    $userid = $request->getHeader('user');

    $response = $json;
    $response = $this->validateInputs($request, $response);
    if($response['status'] == 'failed'){
      return json_encode($response);
    }

    $projectTask = ProjectTask::where('id', '=', $json['id'])
                      ->update([
                        'code' => $json['code'],
                        'description' => $json['description'],
                        'mod_units_id' => $json['unit']['id'],
                        'quantity' => $json['quantity'],
                        'rate' => $json['rate'],
                        'estimate_days' => $json['estimate_days'],
                        'sys_auth_user_id' => $userid[0]
                      ]);

    // $response = $projectTask;
    $response['status'] = 'success';
    return json_encode($response);
  }

  public function deleteTask($request, $response){
    $json = $request->getParsedBody();

    ProjectTask::find($json['id'])->delete();

    $response = $json;
    $response['status'] = 'success';
    return json_encode($response);
  }

  public function validateInputs($request, $response){
    $validation = $this->validator->validate($request, [
      'code' => v::notEmpty()->alpha(),//->projectTaskAvailable(),
      'description' => v::alpha(),
    ]);

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
    }else{
      $response['status'] ='success';
    }
    return $response;
  }
}
