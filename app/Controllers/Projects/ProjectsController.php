<?php
namespace App\Controllers\Projects;

use App\Controllers\Controller;

class ProjectsController extends Controller{
  public function getAll($request, $response){
    $projects = $this->db::table('mod_proj_base as pb')
                    ->select('id', 'name', 'estimation', 'amount')
                    ->get();
    return json_encode($projects);                
  }
}
