<?php
namespace App\Controllers\Projects;

use App\Controllers\Controller;
use App\Models\Project\ProjectPlan;
use Respect\Validation\Validator as v;

class ProjectPlanController extends Controller{
  public function get($request, $response){
    $projectId = $request->getParam('projectId');

    $plan = $this->db::table('mod_proj_plan')
                ->where('mod_proj_base_id', '=', $projectId)
                ->select('id', 'code', 'description', 'mod_proj_base_id')
                ->orderBy('code', 'asc')
                ->get();

    foreach($plan as $planItem){
      $planItem->tasks = $this->projectTasks->getAll($planItem->id);
    }

    $res = [];
    $res['planItems'] = $plan;
    return json_encode($res);
  }

  public function getItem($request, $response){
    $planId = $request->getParam('planId');
    $plan = ProjectPlan::find($planId);
    return json_encode($plan);
  }

  public function createItem($request, $response){
    $projectId = $request->getParam('projectId');
    $json = $request->getParsedBody();

    $response = $json;
    $response = $this->validateInputs($request, $response);
    if($response['status'] == 'failed'){
      return json_encode($response);
    }

    $projectPlan = ProjectPlan::create([
      'mod_proj_base_id' => $projectId,
      'code' => strtoupper($json['code']),
      'description' => $json['description']
    ]);

    $response = $projectPlan;
    $response['status'] = 'success';
    return json_encode($response);
  }

  public function updateItem($request, $response){
    $json = $request->getParsedBody();

    $response = $json;
    $response = $this->validateInputs($request, $response);
    if($response['status'] == 'failed'){
      return json_encode($response);
    }

    $projectPlan = ProjectPlan::where('id', '=', $json['id'])
                            ->update([
                              'description' => $json['description']
                            ]);

    $response['status'] = 'success';
    return json_encode($response);
  }

  public function deleteItem($request, $response){
    $json = $request->getParsedBody();

    $response = $json;

    ProjectPlan::find($json['id'])->delete();
    $response['status'] = 'success';
    return json_encode($response);
  }

  public function validateInputs($request, $response){
    $validation = $this->validator->validate($request, [
      'code' => v::notEmpty()->alpha(),
      'description' => v::alpha(),
    ]);

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
    }else{
      $response['status'] ='success';
    }
    return $response;
  }
}
