<?php
namespace App\Controllers\Clients;

use App\Controllers\Controller;
use App\Models\Client;

class ClientsController extends Controller{

  public function getClients($request, $response){
    $clients = $this->db::table('mod_client_base as cb')
                  ->leftJoin('mod_client_people as cp', 'cp.mod_client_base_id', '=', 'cb.id')
                  ->leftJoin('mod_proj_base as pb', 'pb.mod_client_base_id', '=', 'cb.id')
                  ->groupBy('cb.id')
                  ->select('cb.id',
                           'cb.name',
                          'cb.tel_office',
                          $this->db::raw('count(cp.id) as people'),
                          $this->db::raw('count(distinct pb.id) as projects'))
                  ->get();
    return json_encode($clients);
  }
}
