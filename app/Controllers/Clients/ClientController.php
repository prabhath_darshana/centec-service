<?php
namespace App\Controllers\Clients;

use App\Controllers\Controller;
use App\Models\Client\Client;
use App\Models\Client\ClientPeople;
use App\Models\People\Profile;
use Respect\Validation\Validator as v;

class ClientController extends Controller{
  public function getClient($request, $response){
    $clientId = $request->getParam('clientId');
    $client = Client::where('id', $clientId)->first();
    return json_encode($client);
  }

  public function createClient($request, $response){
    $json = $request->getParsedBody();

    $response = $json;
    $validation = $this->validator->validate($request, [
      'email' => v::noWhitespace()->email(),
      'name' => v::notEmpty()->alpha()->clientAvailable(),
      'tel_office' => v::digit(),
      'fax_office' => v::digit(),
    ]);

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
      return json_encode($response);
    }

    $client = Client::create([
      'name' => $json['name'],
      'email' => $json['email'],
      'address_line1' => $json['address_line1'],
      'address_line2' => $json['address_line2'],
      'address_line3' => $json['address_line3'],
      'tel_office' => $json['tel_office'],
      'fax_office' => $json['fax_office'],
    ]);

    $response['status'] ='success';
    return json_encode($response);
  }

  public function updateClient($request, $response){
    $json = $request->getParsedBody();

    $response = $json;
    $validation = $this->validator->validate($request, [
      'email' => v::noWhitespace()->email(),
      'name' => v::notEmpty()->alpha(),
      'tel_office' => v::digit(),
      'fax_office' => v::digit()
    ]);

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
      return json_encode($response);
    }

    $client = Client::where('id', $json['id'])
                    ->update([
                        'name' => $json['name'],
                        'email' => $json['email'],
                        'address_line1' => $json['address_line1'],
                        'address_line2' => $json['address_line2'],
                        'address_line3' => $json['address_line3'],
                        'tel_office' => $json['tel_office'],
                        'fax_office' => $json['fax_office'],
                      ]);

    $response['status'] ='success';
    return json_encode($response);
  }

  public function deleteClient($request, $response){
    $json = $request->getParsedBody();

    $this->db::transaction(function() use($json){
      $client = Client::find($json['id']);

      $clientPeople = ClientPeople::where('mod_client_base_id', '=', $client['id'])->get();

      foreach($clientPeople as $person){
        $person = ClientPeople::find($person['id']);
        $profile = Profile::find($person['sys_people_profile_id']);
        $profile->delete();
        $person->delete();
      }
      $client->delete();
    });

    $response = $json;
    $response['status'] ='success';
    return json_encode($response);
  }
}
