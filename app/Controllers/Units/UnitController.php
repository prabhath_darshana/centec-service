<?php
namespace App\Controllers\Units;

use App\Controllers\Controller;
use App\Models\Unit\Unit;
use Respect\Validation\Validator as v;

class UnitController extends Controller{
  public function create($request, $response){
    $json = $request->getParsedBody();

    $response = $json;
    $validation = $this->validator->validate($request, [
      'name' => v::notEmpty()->alpha()->unitAvailable(),
    ]);

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
      return json_encode($response);
    }

    $category = Unit::create([
      'name'=>$json['name']
    ]);

    $respone['id'] = $category['id'];
    $response['status'] ='success';
    return json_encode($response);
  }

  public function delete($request, $response){
    $json = $request->getParsedBody();

    $response = $json;
    $category = Unit::find($json['id']);
    $category->delete();

    $response['status'] ='success';
    return json_encode($response);
  }

  public function getAll($request, $response){
    $categories = Unit::orderBy('id', 'desc')->get();
    return json_encode($categories);
  }
}
