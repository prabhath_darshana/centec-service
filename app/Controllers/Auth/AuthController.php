<?php
namespace App\Controllers\Auth;

use App\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\People\Profile;
use App\Modules\Auth\JWTGenerator;

class AuthController extends Controller{
  public function getJwtVerify($request, $response){
    return json_encode(['status'=>'success']);
  }

  public function postSignIn($request, $response){
    $json = $request->getParsedBody();
    if($this->auth->attempt($json['email'], $json['password'])){
      $user = User::where('email',$json['email'])->first();
      $res = ['status' => 'success',
              'X_TOKEN' => $this->jwt->create($_SESSION['user']),
              'user' => $user->getUserData()
            ];
      return json_encode($res);
    }else{
      return json_encode(['status' => 'failed']);
    }
  }

  public function getSignOut($request, $response){
    var_dump($request->getAttribute('jwt'));
    return "signout";
  }

  public function postSignUp($request, $response){
    $json = $request->getParsedBody();

    $this->db::transaction(function() use ($json){
      $userProfile = Profile::create([
        'first_name' => $json['username'],
        'email' => $json['email'],
      ]);

      $user = User::create([
        'email' => $json['email'],
        'username' => $json['username'],
        'password' => password_hash($json['password'], PASSWORD_DEFAULT, ['cost' => 10]),
        'sys_people_profile_id'=>$userProfile->id,
      ]);
    });

    $success = [
      'status' => 'success'
    ];

    return json_encode($success);
  }

  public function getCurrentUser($request, $response){
    $token = $request->getParam('token');
    $jwtdecoded = $this->jwt->decode($token);
    $userid = $jwtdecoded->data->userid;
    $user = User::where('id',$userid)->first();
    return json_encode($user->getUserData());
  }
}
