<?php
namespace App\Controllers\Employees;

use App\Controllers\Controller;

class EmployeesController extends Controller{
  public function getEmployees($request, $response){
    $employees = $this->db::table('mod_emp_base as eb')
                    ->join('sys_people_profile as pp', 'pp.id', '=', 'eb.sys_people_profile_id')
                    ->join('mod_emp_category as ec', 'ec.id', '=', 'eb.mod_emp_category_id')
                    ->select('eb.id',
                              'ec.name as category',
                              'pp.first_name' ,
                              'pp.last_name',
                              'pp.tel_mobile',
                              'eb.balance_payment')
                    ->get();
    return json_encode($employees);
  }
}
