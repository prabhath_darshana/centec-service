<?php
namespace App\Controllers\Employees;

use App\Controllers\Controller;
use App\Models\Employee\EmpCategory;
use Respect\Validation\Validator as v;

class EmpCategoryController extends Controller{
  public function createEmpCategory($request, $response){
    $json = $request->getParsedBody();

    $response = $json;
    $validation = $this->validator->validate($request, [
      'category' => v::notEmpty()->alpha()->empCategoryAvailable(),
    ]);

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
      return json_encode($response);
    }

    $category = EmpCategory::create([
      'name'=>$json['category']
    ]);

    $respone['id'] = $category['id'];
    $response['status'] ='success';
    return json_encode($response);
  }

  public function deleteCategory($request, $response){
    $json = $request->getParsedBody();

    $response = $json;
    $category = EmpCategory::find($json['id']);
    $category->delete();

    $response['status'] ='success';
    return json_encode($response);
  }

  public function getAllCategories($request, $response){
    $categories = EmpCategory::orderBy('id', 'desc')->get();
    return json_encode($categories);
  }
}
