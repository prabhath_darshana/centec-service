<?php
namespace App\Controllers\Employees;

use App\Controllers\Controller;
use Respect\Validation\Validator as v;
use App\Models\People\Profile;
use App\Models\Employee\Employee;

class EmployeeController extends Controller{

  public function getEmployee($request, $respone){
    $employeeId = $request->getParam('employeeId');
    $employee = Employee::find($employeeId)->getEmployee();
    return json_encode($employee);
  }

  public function createEmployee($request, $response){
    $json = $request->getParsedBody();
    $user = $request->getHeader('user');

    $response = $json;
    $response['user'] = $user;
    $response['category'] = $response['category']['name'];
    $validation = $this->validator->validate($request, [
      'profile.first_name' => v::notEmpty()->alpha(),
      'category'=> v::notEmpty(),
      'profile.last_name' => v::alpha(),
      'profile.tel_mobile' => v::digit(),
      'pay_per_day' => v::notEmpty()->digit(),
    ]);

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
      return json_encode($response);
    }

    $this->db::transaction(function() use ($json, $user, $response){
      $person = Profile::create([
        'first_name' => $json['first_name'],
        'last_name' => $json['last_name'],
        // 'dob' => $json['dob'],
        'email' => $json['email'],
        'address_line1' => $json['address_line1'],
        'address_line2' => $json['address_line2'],
        'address_line3' => $json['address_line3'],
        'tel_home' => $json['tel_home'],
        'tel_mobile' => $json['tel_mobile'],
        'tel_office' => $json['tel_office'],
        // 'gender' => $json['gender']
      ]);

      $project = Employee::create([
        'mod_emp_category_id'=>$json['category']['id'],
        'sys_people_profile_id'=>$person['id'],
        'sys_auth_user_id'=>$user[0],
        'pay_per_day' => $json['pay_per_day'],
      ]);

      $response['id']=$person['id'];
    });

    $response['status'] ='success';
    return json_encode($response);
  }

  public function deleteEmployee($request, $response){
    $json = $request->getParsedBody();

    $this->db::transaction(function() use($json){
      $employee = Employee::find($json['id']);
      $person = Profile::find($employee['sys_people_profile_id']);

      $person->delete();
      $employee->delete();
    });

    $response = $json;
    $response['status'] ='success';
    return json_encode($response);
  }
}
