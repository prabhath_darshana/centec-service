<?php
namespace App\Controllers\Employees;

use App\Controllers\Controller;
use App\Modules\Employee\Work;
use App\Models\Employee\EmpWork;
use App\Models\Employee\Employee;
use Respect\Validation\Validator as v;

class EmployeeWorkController extends Controller{
  public function createWork($request, $response){
    $json = $request->getParsedBody();
    $user = $request->getHeader('user');

    $response = $json;
    $response['user'] = $user;
    $response['project'] = $response['project']['name'];
    $validation = $this->validator->validate($request, [
      'project' => v::notEmpty(),
      'work_description'=>v::notEmpty(),
      'date'=> v::notEmpty(),
      'daily_payable' => v::digit(),
      'day_paid' => v::digit(),
    ]);

    if($validation->failed()){
      $response['status'] ='failed';
      $response['errors'] = $validation->errors;
      return json_encode($response);
    }

    $work = new Work($this->container);
    $json = $work->create($json, $user);

    return json_encode($json);
  }

  public function getWorkLog($request, $response){
    $params = $request->getParams();

    $work = new Work($this->container);
    $workLog = $work->getWorkLog($params);
    return json_encode($workLog);
  }

  public function deleteWork($request, $response){
    $json = $request->getParsedBody();
    $user = $request->getHeader('user');

    $work = new Work($this->container);
    $json = $work->delete($json, $user);
    return json_encode($json);
  }

  public function updateWork($request, $response){
    $json = $request->getParsedBody();
    $user = $request->getHeader('user');

    $work = new Work($this->container);
    $json = $work->update($json, $user);
    return json_encode($json);
  }

  public function doPayment($request, $response){
    $json = $request->getParsedBody();
    $user = $request->getHeader('user');

    $work = new Work($this->container);
    $json = $work->doPayment($json, $user);
    return json_encode($json);
  }
}
