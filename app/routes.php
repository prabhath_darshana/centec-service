<?php
$app->get('/auth/check', 'AuthController:getJwtVerify')->setName('auth.jwtVerify');
$app->post('/auth/signin', 'AuthController:postSignIn')->setName('auth.signin');
$app->get('/auth/signout', 'AuthController:getSignOut')->setName('auth.signout');
$app->post('/auth/signup', 'AuthController:postSignUp')->setName('auth.signup');
$app->get('/auth/user', 'AuthController:getCurrentUser')->setName('auth.getcurrentuser');

$app->get('/people/profile', 'PeopleController:getProfile')->setName('people.getprofile');
$app->post('/people/create', 'PeopleController:create');
$app->put('/people/update', 'PeopleController:update');
$app->delete('/people/delete', 'PeopleController:delete');
$app->post('/people/updateWithFile', 'PeopleController:updateWithProfilePic'); //since file uploader use this
$app->get('/people/getall', 'PeopleController:getAllPeopleAssociatedWith');

$app->post('/client/create', 'ClientController:createClient');
$app->get('/clients/getall', 'ClientsController:getClients');
$app->get('/client/get', 'ClientController:getClient');
$app->post('/client/update', 'ClientController:updateClient');
$app->delete('/client/delete', 'ClientController:deleteClient');

$app->post('/project/create', 'ProjectController:create');
$app->put('/project/update', 'ProjectController:update');
$app->get('/projects/getall', 'ProjectsController:getAll');
$app->get('/project/get', 'ProjectController:get');
$app->post('/project/plan/item/create', 'ProjectPlanController:createItem');
$app->put('/project/plan/item/update', 'ProjectPlanController:updateItem');
$app->delete('/project/plan/item/delete', 'ProjectPlanController:deleteItem');
$app->get('/project/plan/item/get', 'ProjectPlanControler:getItem');
$app->get('/project/plan/get', 'ProjectPlanController:get');
$app->post('/project/task/create', 'ProjectTaskController:createTask');
$app->get('/project/task/getall', 'ProjectTaskController:getAll');
$app->put('/project/task/update', 'ProjectTaskController:updateTask');
$app->delete('/project/task/delete', 'ProjectTaskController:deleteTask');

$app->post('/employee/create', 'EmployeeController:createEmployee');
$app->post('/employee/category/create', 'EmpCategoryController:createEmpCategory');
$app->get('/employee/category/getall', 'EmpCategoryController:getAllCategories');
$app->delete('/employee/category/delete', 'EmpCategoryController:deleteCategory');
$app->get('/employees/getall', 'EmployeesController:getEmployees');
$app->get('/employee/get', 'EmployeeController:getEmployee');
$app->delete('/employee/delete', 'EmployeeController:deleteEmployee');
$app->post('/employee/work/create', 'EmployeeWorkController:createWork');
$app->get('/employee/work/log', 'EmployeeWorkController:getWorkLog');
$app->delete('/employee/work/delete', 'EmployeeWorkController:deleteWork');
$app->put('/employee/work/update', 'EmployeeWorkController:updateWork');
$app->put('/employee/work/dopayment', 'EmployeeWorkController:doPayment');

$app->post('/supplier/create', 'SupplierController:createSupplier');
$app->get('/suppliers/getall', 'SuppliersController:getAllSuppliers');
$app->delete('/supplier/delete', 'SupplierController:deleteSupplier');
$app->get('/supplier/get', 'SupplierController:getSupplier');
$app->put('/supplier/update', 'SupplierController:updateSupplier');
$app->post('/supplier/po/create', 'SupplierPoController:createSupplierPo');

$app->post('/unit/create', 'UnitController:create');
$app->get('/unit/getall', 'UnitController:getAll');
$app->delete('/unit/delete', 'UnitController:delete');
