<?php
namespace App\Validation\Exceptions;

use \Respect\Validation\Exceptions\ValidationException;

class ClientAvailableException extends ValidationException{
  public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'Client is already exists',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '',
        ],
    ];
}
