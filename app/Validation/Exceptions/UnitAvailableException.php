<?php
namespace App\Validation\Exceptions;

use \Respect\Validation\Exceptions\ValidationException;

class UnitAvailableException extends ValidationException{
  public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'Unit is already exists',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '',
        ],
    ];
}
