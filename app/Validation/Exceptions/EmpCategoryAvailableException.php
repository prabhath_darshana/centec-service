<?php
namespace App\Validation\Exceptions;

use \Respect\Validation\Exceptions\ValidationException;

class EmpCategoryAvailableException extends ValidationException{
  public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'Category is already exists',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '',
        ],
    ];
}
