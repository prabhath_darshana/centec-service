<?php
namespace App\Validation\Exceptions;

use \Respect\Validation\Exceptions\ValidationException;

class ProjectAvailableException extends ValidationException{
  public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'Project task code is used. Please use different name',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '',
        ],
    ];
}
