<?php
namespace App\Validation\Rules;

use Respect\validation\Rules\AbstractRule;
use App\Models\Employee\EmpCategory;

class EmpCategoryAvailable extends AbstractRule{
  public function validate($input){
    return EmpCategory::where('name', $input)->count() === 0;
  }
}
