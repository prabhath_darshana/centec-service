<?php
namespace App\Validation\Rules;

use App\Models\Client\Client;
use Respect\validation\Rules\AbstractRule;

class ClientAvailable extends AbstractRule{
  public function validate($input){
    return Client::where('name', $input)->count() === 0;
  }
}
