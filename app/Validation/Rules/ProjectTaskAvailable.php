<?php
namespace App\Validation\Rules;

use Respect\validation\Rules\AbstractRule;
use App\Models\Project\ProjectTask;

class ProjectTaskAvailable extends AbstractRule{
  public function validate($input){
    return ProjectTask::where('code', $input)->count() === 0;
  }
}
