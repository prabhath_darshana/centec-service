<?php
namespace App\Validation\Rules;

use Respect\validation\Rules\AbstractRule;
use App\Models\Project\Project;

class ProjectAvailable extends AbstractRule{
  public function validate($input){
    return Project::where('name', $input)->count() === 0;
  }
}
