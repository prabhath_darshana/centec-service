<?php
namespace App\Validation\Rules;

use Respect\validation\Rules\AbstractRule;
use App\Models\Unit\Unit;

class UnitAvailable extends AbstractRule{
  public function validate($input){
    return Unit::where('name', $input)->count() === 0;
  }
}
