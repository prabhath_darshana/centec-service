<?php
namespace App\Validation;

// use Respect\Validation\Validator as Respect;
use \Exception as Exception;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator{
  public $errors;
  public function validate($request, array $rules){

    foreach ($rules as $field => $rule) {
      try{
          if(strpos($field, ".")){
              $arr = explode('.', $field);
              $rule->setName(ucfirst($arr[1]))->assert($request->getParam($arr[0])[$arr[1]]);
              // var_dump($request->getParam($arr[0])[$arr[1]]);
          }else{
              $rule->setName(ucfirst($field))->assert($request->getParam($field));
          }
      }catch(NestedValidationException $e){
        $this->errors[$field] = $e->getMessages();
      }
    }

    $_SESSION['errors'] = $this->errors;

    return $this;
  }

  public function failed(){
    return !empty($this->errors);
  }
}
